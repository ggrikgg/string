#include "stdafx.h"

#include "String.h"




namespace mystring {

	template<typename T>
	constexpr size_t MaxSize()
	{
		return  (size_t(-1) / sizeof(T) - 1) / 2;
	}
	//	template<typename StrType>
	//	class String_view_iterator
	//	{
	//	public:
	//		using pointer = const StrType *;
	//		using reference = const  StrType&;
	//	private:
	//		constexpr _String_view_iterator() _NOEXCEPT
	//			:Myptr_(nullptr)
	//		{
	//		}
	//		constexpr explicit _String_view_iterator(const pointer _Ptr) noexcept
	//			: _Myptr(_Ptr)
	//		{	// initialize a basic_string_view::const_iterator
	//		}
	//	
	//		pointer Myptr_;
	//
	//	public:
	//		constexpr reference operator*() const noexcept
	//		{	// return designated object
	//#if _ITERATOR_DEBUG_LEVEL >= 1
	//			_IDL_VERIFY(_Mydata, "cannot dereference value-initialized string_view iterator");
	//			_IDL_VERIFY(_Myoff < _Mysize, "cannot dereference end string_view iterator");
	//			return (_Mydata[_Myoff]);
	//#else /* ^^^ _ITERATOR_DEBUG_LEVEL >= 1 ^^^ // vvv _ITERATOR_DEBUG_LEVEL == 0 vvv */
	//			return (*_Myptr);
	//#endif /* _ITERATOR_DEBUG_LEVEL */
	//		}
	//
	//

	//	};



	template<typename StrType>
	String<StrType>::String(const StrType* obj)
	{
		size_t i;
		for (i = 0; obj[i] != '\0'; ++i);
		if (i > S_local_capacity_ + 1)
		{
			difstr_ = new srep;
			if (MaxSize<StrType>() > i)
			{
				capacity_ = i * 2;
				difstr_->length_ = i;
			}
			else
			{
				capacity_ = MaxSize<StrType>() - 1;
				difstr_->length_ = MaxSize<StrType>() - 1;
			}
			difstr_->str_ = new StrType[capacity_];
			for (size_t j = 0; j < difstr_->length_ + 1; ++j)
				difstr_->str_[j] = obj[j];
			pointer_ = difstr_->str_;
		}
		else
		{
			for (size_t j = 0; j < i + 1; ++j)
				local_buf_[j] = obj[j];
			length_ = i;
		}
	}

	template<typename StrType>
	String<StrType>::String(const String& obj)
	{
		if (obj.pointer_ == obj.local_buf_)
		{
			size_t i = 0;
			length_ = obj.length_;
			while (i < length_ + 1)
				local_buf_[i] = obj.local_buf_[i++];
		}
		else
		{
			obj.difstr_->count_++;
			difstr_ = obj.difstr_;
			pointer_ = difstr_->str_;
			capacity_ = obj.capacity_;
		}
	}

	template<typename StrType>
	String<StrType>::String(String&& temp) noexcept
	//	: pointer_(temp.pointer_)
		: length_(temp.length_)
	//	, difstr_(temp.difstr_)
		, capacity_(temp.capacity_)
	{
		if (pointer_ != local_buf_)
			if (difstr_ != nullptr)
			{
				if (--difstr_->count_ == 0)
				{
					if (difstr_->str_ != nullptr)
					{
						delete[] difstr_->str_;
						difstr_->str_ = nullptr;
					}
					delete difstr_;
				}
			}
		pointer_ = temp.pointer_;
		difstr_ = temp.difstr_;
		if (temp.pointer_ == temp.local_buf_)
		{
			pointer_ = local_buf_;
		}
		else
		{
			temp.pointer_ = temp.local_buf_;
			temp.difstr_ = nullptr;
			temp.length_ = 0;
		}
	}

	template<typename StrType>
	String<StrType>::~String()
	{
		if (pointer_ != local_buf_)
			if (difstr_ != nullptr)
			{
				if (--difstr_->count_ == 0)
				{
					if (difstr_->str_ != nullptr)
					{
						delete[] difstr_->str_;
						difstr_->str_ = nullptr;
					}
					delete difstr_;
				}
			}
	}

	template<typename StrType>
	String<StrType>& String<StrType>::operator=(const String<StrType>& obj)
	{
		if (pointer_ == local_buf_)
		{
			if (obj.pointer_ == obj.local_buf_)
			{
				length_ = obj.length_;
				for (size_t i = 0; i < length_ + 1; ++i)
					local_buf_[i] = obj.local_buf_[i];
				return *this;
			}
			else
			{
				difstr_ = obj.difstr_;
				difstr_->count_++;
				pointer_ = difstr_->str_;
				capacity_=obj.capacity_;
				return *this;
			}
		}
		else
		{

			if (--difstr_->count_ == 0)
			{
				delete[] difstr_->str_;
				difstr_->str_ = nullptr;
				delete difstr_;
				difstr_ = nullptr;
			}

			if (obj.pointer_ == obj.local_buf_)
			{
				length_ = obj.length_;
				pointer_ = local_buf_;
				for (int i = 0; i < length_+1; ++i)
				{
					local_buf_[i] = obj.local_buf_[i];
				}
				return *this;
			}
			else
			{
				difstr_ = obj.difstr_;
				capacity_ = obj.capacity_;
				pointer_ = difstr_->str_;
				obj.difstr_->count_++;
				return *this;
			}
		}
	}

	template<typename StrType>
	String<StrType>& String<StrType>::operator=(const StrType*)//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	{
		return *this;

	}

	template<typename StrType>
	String<StrType>& String<StrType>::operator=(String<StrType>&& temp) noexcept
	{
		if (pointer_ != local_buf_)
		{
			if (--difstr_->count_ == 0)
			{
				if (difstr_->str_ != nullptr)
				{
					delete[] difstr_->str_;
					difstr_->str_ = nullptr;
				}
				delete difstr_;
			}
			difstr_ = nullptr;
		}

		if (temp.pointer_ == temp.local_buf_)
		{
			pointer_ = local_buf_;
			length_ = temp.length_;
			capacity_ = temp.capacity_;
			for (size_t i = 0; i < temp.length_; ++i)
				local_buf_[i] = temp.local_buf_[i];
			return *this;
		}

		difstr_ = temp.difstr_;
		temp.difstr_ = nullptr;
		capacity_ = temp.capacity_;
		pointer_ = difstr_->str_;
		temp.pointer_ = temp.local_buf_;
		temp.length_ = 0;
		return *this;
	}



	template<typename StrType>
	String<StrType>& String<StrType>::operator+=(const String<StrType>& obj)
	{
		if (pointer_ == local_buf_ && obj.pointer_ == obj.local_buf_)
		{
			size_t temp = length_ + obj.length_;
			if (temp <= S_local_capacity_)
			{
				for (size_t i = length_; i < temp + 1; ++i)
					local_buf_[i] = obj.local_buf_[i - length_];
				length_ = temp;
			}
			else
			{
				size_t temp2 = 2 * temp;
				difstr_ = new srep;
				if (temp2 > MaxSize<StrType>())
				{
					capacity_ = MaxSize<StrType>() - 1;
					difstr_->length_ = capacity_;
					for (size_t j = 0; j < length_ && j < capacity_; ++j)
						difstr_->str_[j] = local_buf_[j];
					for (size_t i = 0, j = length_; j < temp && j < capacity_; ++j, ++i)
						difstr_->str_[j] = obj.local_buf_[i];
				}
				else
				{
					difstr_->length_ = temp;
					difstr_->str_ = new StrType[temp2 + 1];
					for (size_t j = 0; j < length_; ++j)
						difstr_->str_[j] = local_buf_[j];
					for (size_t i = 0, j = length_; j < temp+1; ++j, ++i)
						difstr_->str_[j] = obj.local_buf_[i];
				}
				capacity_ = temp2;
				pointer_ = difstr_->str_;
			}
			return *this;
		}

		if (pointer_ == local_buf_)
		{
			difstr_ = new srep;
			size_t temp = length_ + obj.difstr_->length_;
			size_t tempcap;
			if (temp > MaxSize<StrType>())
			{
				tempcap = MaxSize<StrType>() - 1;
				difstr_->length_ = capacity_;
			}
			else
			{
				tempcap = 2 * temp;
				difstr_->length_ = temp;
			}
			difstr_->str_ = new StrType[tempcap + 1];
			for (size_t j = 0; j < length_ && j < tempcap; ++j)
				difstr_->str_[j] = local_buf_[j];
			for (size_t i = 0, j = length_; j < temp + 1 && j < tempcap; ++j, ++i)
				difstr_->str_[j] = obj.difstr_->str_[i];
			capacity_ = tempcap;
			pointer_ = difstr_->str_;
			return *this;
		}
		size_t tempcap, temp;
		if (obj.pointer_ == obj.local_buf_)
		{
			temp = obj.length_ + difstr_->length_;
			if (difstr_->count_ == 1)
				if (temp + 1 <= capacity_)
				{
					for (size_t i = 0, j = difstr_->length_; j < temp + 1; ++j, ++i)
						difstr_->str_[j] = obj.local_buf_[i];
					difstr_->length_ = temp;
					return *this;
				}
			if (temp > MaxSize<StrType>())
			{
				capacity_ = MaxSize<StrType>() - 1;
				tempcap = capacity_;
			}
			else
			{
				capacity_ = 2 * temp;
				tempcap = temp;
			}
			StrType*tempstr = new StrType[capacity_ + 1];
			for (size_t j = 0; j < difstr_->length_ && j < capacity_; ++j)
				tempstr[j] = difstr_->str_[j];
			for (size_t i = 0, j = difstr_->length_; j < temp + 1 && j < capacity_; ++j, ++i)
				tempstr[j] = obj.local_buf_[i];
			if (--difstr_->count_ == 0)
			{
				if (difstr_->str_ != nullptr)
				{
					delete[] difstr_->str_;
					difstr_->str_ = nullptr;
				}
				delete difstr_;

			}
			difstr_ = new srep;
			difstr_->length_ = tempcap;
			difstr_->str_ = tempstr;
			pointer_ = difstr_->str_;
			return *this;
		}
		else
		{
			temp = obj.difstr_->length_ + difstr_->length_;
			if (difstr_->count_ == 1)
				if (temp + 1 <= capacity_)
				{
					for (size_t i = 0, j = difstr_->length_; j < temp + 1; ++j, ++i)
						difstr_->str_[j] = obj.difstr_->str_[i];
					difstr_->length_ = temp;
					return *this;
				}
			if (temp > MaxSize<StrType>())
			{
				capacity_ = MaxSize<StrType>() - 1;
				tempcap = capacity_;
			}
			else
			{
				capacity_ = 2 * temp;
				tempcap = temp;
			}
			StrType* tempstr = new StrType[capacity_ + 1];
			for (size_t j = 0; j < difstr_->length_ && j < capacity_; ++j)
				tempstr[j] = difstr_->str_[j];
			for (size_t i = 0, j = difstr_->length_; j < temp + 1 && j < capacity_; ++j, ++i)
				tempstr[j] = obj.difstr_->str_[i];
			if (--difstr_->count_ == 0)
			{
				if (difstr_->str_ != nullptr)
				{
					delete[] difstr_->str_;
					difstr_->str_ = nullptr;
				}
				delete difstr_;

			}
			difstr_ = new srep;
			difstr_->length_ = tempcap;
			difstr_->str_ = tempstr;
			pointer_ = difstr_->str_;
			return *this;
		}

	}

	template<typename StrType>
	String<StrType> operator+(const String<StrType>& obj1, const String<StrType>& obj2)
	{
		String<StrType> str(obj1);
		str += obj2;
		return str;
	}

	template<typename StrType> inline
	bool operator==(const String<StrType>& obj1, const String<StrType>& obj2)
	{
		if (obj1.pointer_ == obj1.local_buf_^ obj2.pointer_ == obj2.local_buf_) return false;
		if (obj1.pointer_ == obj1.local_buf_)
		{
			if (obj1.length_ != obj2.length_) return false;
			for (size_t i = 0; i < obj1.length_; ++i)
				if (obj1.local_buf_[i] != obj2.local_buf_[i])
					return false;
			return true;
		}
		if (obj1.difstr_ == obj2.difstr_) return true;
		if (obj1.difstr_ == nullptr || obj2.difstr_ == nullptr) return false;
		if (obj1.difstr_->str_ == obj2.difstr_->str_) return true;
		if (obj1.difstr_->str_ == nullptr || obj2.difstr_->str_ == nullptr) return false;
		if (obj1.difstr_->length_ != obj2.difstr_->length_) return false;
		for (size_t i = 0; i < obj1.difstr_->length_; ++i)
			if (obj1.difstr_->str_[i] != obj2.difstr_->str_[i]) return false;
		return true;
	}

	template<typename T> inline
	bool operator==(const String<T>& obj1, const T* rhs)
	{
		if (obj1.pointer_ == obj1.local_buf_)
		{
			size_t temp = strlen(rhs);
			if (obj1.length_ != temp)
				return false;
			for (size_t i = 0; i < temp; ++i)
				if (obj1.local_buf_[i] != rhs[i])
					return false;
		}
		else
		{
			if (obj1.difstr_ == nullptr)
				return false;
			if (obj1.difstr_->str_ == nullptr)
				return false;
			size_t temp = strlen(rhs);
			if (obj1.difstr_->length_ != temp)
				return false;
			for (size_t i = 0; i < temp; ++i)
				if (rhs[i] != obj1.difstr_->str_[i])
					return false;
		}
		return true;
	}


	template<typename StrType> inline
		bool operator!=(const String<StrType>& obj1, const String<StrType>& obj2)
	{
		return !(obj1 == obj2);
	}

	template<typename StrType> inline
		StrType& String<StrType>::operator[](const size_t i)
	{
		StrType error = '\0';
		if (i < 1)
			throw("out of range");
		if (pointer_ == local_buf_)
		{
			if (length_ > i)
				return local_buf_[i];
			else
				throw("out of range");;
		}
		else
		{
			if (difstr_ == nullptr)
				throw("out of range");;
			if (difstr_->length_ > i)
				return difstr_->str_[i];
		}
		throw("out of range");;
	}

	template<typename StrType> inline
		StrType String<StrType>::operator[](const size_t i) const
	{
		StrType error = '\0';
		if (i < 1)
			throw("out of range");
		if (pointer_ == local_buf_)
		{
			if (length_ > i)
				return local_buf_[i];
			else
				throw("out of range");;
		}
		else
		{
			if (difstr_ == nullptr)
				throw("out of range");;
			if (difstr_->length_ > i)
				return difstr_->str_[i];
		}
		throw("out of range");;
	}

	template<typename StrType>
	std::ostream& operator<< <>(std::ostream& os, const String<StrType>& obj)
	{
		if (obj.pointer_ == obj.local_buf_)
			return os << obj.local_buf_;
		return os << obj.difstr_->str_;
	}

	template<typename T>
	std::istream& operator>><>(std::istream& is, String<T>& obj)
	{
		if (256 > obj.MaxSize<T>())
		{
			//error have not enough memory
			return is;
		}
		T* buff = new T[256];

		is.getline(buff, 256);
		if (obj.pointer_ == obj.local_buf_)
		{
			size_t i = 0;
			while (i <= obj.S_local_capacity_ && (obj.local_buf_[i] = buff[i++]));
			if (buff[i - 1] != '\0')
			{
				obj.difstr_ = new String<T>::srep;
				while (buff[i++] != '\0');
				obj.difstr_->str_ = buff;
				pointer_ = str_;
				obj.capacity_ = 255;
			}
			else
			{
				obj.length_ = i - 1;
			}
		}
		else
		{

			if (obj.difstr_ == nullptr)
			{
				obj.difstr_ = new String<T>::srep;
			}
			else
			{
				if (--obj.difstr_->count_ == 0)
				{
					delete[] obj.difstr_->str_;
					obj.difstr_->str_ = nullptr;
				}
				else
				{
					obj.difstr_ = new String<T>::srep;
				}
			}
			obj.difstr_->count_ = 1;
			obj.difstr_->str_.store(buff);
			obj.capacity_ = 256;
			size_t i = 0;
			while (buff[i] != '\0') i++;
			obj.difstr_->length_ = i;
		}
		return is;
	}
}