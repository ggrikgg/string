#pragma once

#include "iostream"
#include <thread>
#include <atomic>

namespace mystring
{

	constexpr size_t MaxSize();

	template<typename StrType>
	class String;
	template<typename T>
	std::istream& operator>> (std::istream& is, String<T>& obj);
	template<typename StrType>
	std::ostream& operator<< (std::ostream& os, const String<StrType>& obj);



	template<typename StrType>
	class String
	{
		struct srep
		{
			StrType* str_ = nullptr;
			size_t length_ = 0;
			std::atomic<size_t > count_ = 1;
		};
		srep * difstr_;
		enum
		{
			S_local_capacity_ = 15 / sizeof(StrType)
		};

		union
		{
			srep * difstr_;
			size_t 		length_;
		};

		union
		{
			StrType	local_buf_[S_local_capacity_ + 1];
			size_t 		capacity_;
		};
		StrType* pointer_ = local_buf_;

	public:
		String() :length_(0) {};
		explicit String(const StrType*);
		String(const String<StrType>&);
		explicit String(String<StrType>&& temp) noexcept;

		~String();

		size_t length() const
		{
			return pointer_ == local_buf_ ? length_ : difstr_->length_;
		}

		size_t capacity() const
		{
			return pointer_ == local_buf_ ? S_local_capacity_ : capacity_;
		}

		size_t count() const
		{
			return pointer_ == local_buf_ ? 1 : difstr_->count_;
		}
		//auto begin()
		//{
		//	return std::iterator<size_t,StrType>(pointer_);
		//}
		//auto end()
		//{
		//	return std::iterator(pointer_)+5;
		//}
		String<StrType>& operator=(const String<StrType>&);
		String<StrType>& operator=(const StrType*);
		String<StrType>& operator+=(const String<StrType>&);
		String<StrType>& operator=(String<StrType>&& temp) noexcept;
		StrType& operator[](const size_t);
		StrType operator[](const size_t) const;

		template<typename T>
		friend String<T> operator+(const String<T>&, const String<T>&);
		template<typename T>
		friend bool inline operator==(const String<T>&, const String<T>&);
		template<typename T>
		friend bool inline operator==(const String<T>& obj1, const T* rhs);
		template<typename T>
		friend bool inline operator!=(const String<T>&, const String<T>&);
		template<typename T>
		friend std::ostream& operator<< <>(std::ostream&, const String<T>&);
		template<typename T>
		friend std::istream& operator>> <>(std::istream&, String<T>&);



	};
}
