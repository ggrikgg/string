// String.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"


#include "String.h"
#include "String1.cpp"
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "thread"
#include "string"



SCENARIO("Default constructors")
{
	GIVEN("Ampty char string s") {
		mystring::String<char> s;

		THEN("a value count_ must be 1, length must be 0")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == 0);
			REQUIRE(s.capacity() == 15);
		}
	}
}

SCENARIO("Copy constructors char*")
{
	GIVEN("Short char string s") 
	{
		mystring::String<char> s("1234");
		THEN("a value count_ must be 1, length must be 0")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == 4);
			REQUIRE(s.capacity() == 15);
		}
	}
	GIVEN("Long char string s")
	{
		mystring::String<char> s("1234123412341234123412341234");
		THEN("a value count_ must be 1, length must be 0")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == 28);
			REQUIRE(s.capacity() == 56);
		}
	}

}

SCENARIO("Copy constructors object")
{
	GIVEN("Short char string s")
	{
		mystring::String<char> temp("1234");
		mystring::String<char> s=temp;
		THEN("a value count_ must be 1, length must be temp length")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == 4);
			REQUIRE(s.capacity() == 15);
		}
	}
	GIVEN("Long char string s")
	{
		mystring::String<char> temp("1234123412341234123412341234");
		mystring::String<char> s = temp;
		THEN("a value count_ must be 2, length must be temp length")
		{
			REQUIRE(s.count() == 2);
			REQUIRE(s.length() == 28);
			REQUIRE(s.capacity() == 56);
			WHEN("s point at another string")
			{
				mystring::String<char> temp1("1234123412341234123412341234");
				s = temp1;
				THEN("temp count is 1 and s ccount is 2")
				{
					REQUIRE(s.count()==2);
					REQUIRE(temp.count() == 1);
				}
			}
		}
	}
}


SCENARIO("Move constructors")
{
	GIVEN("Short char string s")
	{
		char * tempstr = "1234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s(std::move(temp));
		THEN("value s and temp must be the same")
		{
			REQUIRE(s== tempstr);
			REQUIRE(s.length() == 4);
			REQUIRE(s.capacity() == 15);
			REQUIRE(temp == tempstr);
			REQUIRE(temp.length() == 4);
			REQUIRE(temp.capacity() == 15);
		}
	}
	GIVEN("Long char string s")
	{
		char *tempstr = "1234123412341234123412341234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s(std::move(temp));
		THEN("a value s must be preveus value if temp and temp must be empty")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == 28);
			REQUIRE(s.capacity() == 56);

			REQUIRE(temp.count() == 1);
			REQUIRE(temp.length() == 0);
			REQUIRE(temp.capacity() == 15);
			REQUIRE(temp != s);
		}
		WHEN("few strings point at the same string")
		{
			mystring::String<char> temp1 = s;
			mystring::String<char> temp2 = s;
			mystring::String<char> temp3("123");
			s = std::move(temp3);
			THEN("temp count is 1 and s ccount is 2")
			{
				REQUIRE(temp1.count() == 2);
				REQUIRE(temp1.length() == 28);
				REQUIRE(temp1.capacity() == 56);
				REQUIRE(temp1 == tempstr);

				REQUIRE(s.count() == 1);
			}
		}
	}
}

SCENARIO("Move operator")
{
	GIVEN("Short char string s")
	{
		char * tempstr = "1234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s;
		s= std::move(temp);
		THEN("value s and temp must be the same")
		{
			REQUIRE(s == tempstr);
			REQUIRE(s.length() == 4);
			REQUIRE(s.capacity() == 15);
			REQUIRE(temp == tempstr);
			REQUIRE(temp.length() == 4);
			REQUIRE(temp.capacity() == 15);
		}
	}
	GIVEN("Long char string s")
	{
		char *tempstr = "1234123412341234123412341234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s;
		s=std::move(temp);
		THEN("a value s must be preveus value if temp and temp must be empty")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == 28);
			REQUIRE(s.capacity() == 56);

			REQUIRE(temp.count() == 1);
			REQUIRE(temp.length() == 0);
			REQUIRE(temp.capacity() == 15);
			REQUIRE(temp != s);
		}
		WHEN("few strings point at the same string")
		{
			mystring::String<char> temp1 = s;
			mystring::String<char> temp2 = s;
			mystring::String<char> temp3("123");
			s = std::move(temp3);
			THEN("temp count is 1 and s ccount is 2")
			{
				REQUIRE(temp1.count() == 2);
				REQUIRE(temp1.length() == 28);
				REQUIRE(temp1.capacity() == 56);
				REQUIRE(temp1 == tempstr);

				REQUIRE(s.count() == 1);
			}
		}
	}
}

SCENARIO("Operator +=")
{
	GIVEN("Two short char string s")
	{
		char * tempstr = "1234";
		char * tempstr1 = "12341234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s(tempstr);
		s += temp;
		THEN("Testing values")
		{
			REQUIRE(s == tempstr1);
			REQUIRE(s.length() == 8);
			REQUIRE(s.capacity() == 15);
			REQUIRE(temp == tempstr);
			REQUIRE(temp.length() == 4);
			REQUIRE(temp.capacity() == 15);
		}
	}

	GIVEN("Two short char string s=long string")
	{
		char * tempstr = "123412341234";
		char * tempstr1 = "123412341234123412341234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s(tempstr);
		s += temp;
		THEN("Testing values")
		{
			REQUIRE(s == tempstr1);
			REQUIRE(s.length() == 24);
			REQUIRE(s.capacity() == 48);
			REQUIRE(temp == tempstr);
			REQUIRE(temp.length() == 12);
			REQUIRE(temp.capacity() == 15);
		}
	}

	GIVEN("Short and Long char string s")
	{
		char * tempstr = "1234";
		char * tempstr1 = "123412341234123412341234";
		char * tmp = "1234123412341234123412341234";
		mystring::String<char> temp(tempstr1);
		mystring::String<char> s(tempstr);
		s += temp;
		THEN("Testing values")
		{
			REQUIRE(s == tmp);
			REQUIRE(s.length() ==(strlen(tempstr1)+strlen(tempstr)));
			REQUIRE(s.capacity() ==2*(strlen(tempstr1) + strlen(tempstr)));
			REQUIRE(temp == tempstr1);
			REQUIRE(temp.length() == strlen(tempstr1));
			REQUIRE(temp.capacity() == 2*strlen(tempstr1));
		}
	}

	GIVEN("Long and Short char string s")
	{
		char * tempstr = "1234";
		char * tempstr1 = "123412341234123412341234";
		char * tmp =  "1234123412341234123412341234";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s(tempstr1);
		s += temp;
		THEN("Testing values")
		{
			REQUIRE(s == tmp);
			REQUIRE(s.length() == (strlen(tempstr1) + strlen(tempstr)));
			REQUIRE(s.capacity() == 2 * (strlen(tempstr1)));
			REQUIRE(temp == tempstr);
			REQUIRE(temp.length() == strlen(tempstr));
			REQUIRE(temp.capacity() ==15);
		}
		WHEN("few strings point at the same string")
		{
			mystring::String<char> temp1 = s;
			mystring::String<char> temp2 = s;
			mystring::String<char> temp3("123");
			s += temp3;
			THEN("temp count is 1 and s ccount is 2")
			{
				REQUIRE(s.count() == 1);
				REQUIRE(s.length() == strlen(tempstr1) + strlen(tempstr)+ strlen("123"));
				REQUIRE(s.capacity() == 2 * (strlen(tempstr1) + strlen(tempstr) + strlen("123")));

				REQUIRE(s.count() == 1);
			}
		}
	}


	GIVEN("Long and Long char string s")
	{
		char *tempstr = "1234123412341234123412341234";
		char *tempstr1 = "123412341234123412341234123411";
		mystring::String<char> temp(tempstr);
		mystring::String<char> s(tempstr1);
		s += temp;
		THEN("a value s must be preveus value if temp and temp must be empty")
		{
			REQUIRE(s.count() == 1);
			REQUIRE(s.length() == strlen(tempstr1)+strlen(tempstr));
			REQUIRE(s.capacity() == strlen(tempstr1)*2);
		}
		WHEN("few strings point at the same string")
		{
			mystring::String<char> temp1 = s;
			mystring::String<char> temp2 = s;
			mystring::String<char> temp3("123123123123123123");
			s += temp3;
			THEN("temp count is 1 and s ccount is 2")
			{
				REQUIRE(temp1.count() == 2);
				REQUIRE(s.length() == strlen(tempstr1) + strlen(tempstr)+strlen("123123123123123123"));
				REQUIRE(s.capacity() ==2*(strlen(tempstr1) + strlen(tempstr) + strlen("123123123123123123")));

				REQUIRE(s.count() == 1);
			}
		}
	}
}


SCENARIO("Operator = obj")
{
	GIVEN("two short string")
	{
		char * tempstr = "1234";
		char * tempstr1 = "12341234";
		mystring::String<char> temp(tempstr1);
		mystring::String<char> s(tempstr);
		s = temp;
		THEN("Testing values")
		{
			REQUIRE(s == tempstr1);
			REQUIRE(s.length() == 8);
			REQUIRE(s.capacity() == 15);
			REQUIRE(temp == tempstr1);
			REQUIRE(temp.length() == 8);
			REQUIRE(temp.capacity() == 15);
		}
	}
	GIVEN("short and long string")
	{
		char * tempstr = "1234";
		char * tempstr1 = "12341234123412341234";
		mystring::String<char> s(tempstr);
		mystring::String<char> temp(tempstr1);
		s = temp;
		THEN("Testing values")
		{
			REQUIRE(s == tempstr1);
			REQUIRE(s.length() == strlen("12341234123412341234"));
			REQUIRE(s.capacity() ==2* strlen("12341234123412341234"));
			REQUIRE(temp == tempstr1);
			REQUIRE(s.count() == 2 );

		}
	}
	GIVEN("long and short string")
	{
		char * tempstr1 = "1234";
		char * tempstr = "12341234123412341234";
		mystring::String<char> s(tempstr);
		mystring::String<char> temp(tempstr1);
		s = temp;
		THEN("Testing values")
		{
			REQUIRE(s == tempstr1);
			REQUIRE(s.length() == strlen("1234"));
			REQUIRE(s.capacity() == 15);
			REQUIRE(temp == tempstr1);
			REQUIRE(s.count() == 1);

		}
	}
}




SCENARIO("Try other functions with short string")
{
	GIVEN("Two string with 1234 and 567")
	{
		mystring::String<char> s1("1234");
		mystring::String<char> s2("567");
		mystring::String<char> s3("1234");
		char* c1 = "1234";
		char* c2 = "567";
		REQUIRE(s2 != s3);
		REQUIRE(s1 == s3);
		REQUIRE(!(s1 != s3));
		REQUIRE(s1 == s1);
		REQUIRE(s1.length() == strlen(c1));
		REQUIRE(s1.count() == 1);
		WHEN("Concatenate strings(s1=s2+s3)")
		{
			mystring::String<char> s4;
			s4 = s1 + s2;
			char* c4 = "1234567";
			REQUIRE(s4 == c4);
			REQUIRE(s4.length() == strlen(c4));
			THEN("trying access to index")
			{
				REQUIRE(s4[2] == '3');
			}
		}
		WHEN("Concatenate strings(s1+=s2)")
		{
			mystring::String<char> s4;
			s1 += s2;
			char* c4 = "1234567";
			REQUIRE(s1 == c4);
			REQUIRE(s1.length() == strlen(c4));
			THEN("trying access to index")
			{
				REQUIRE(s1[2] == '3');
			}
		}
		WHEN("Few string have access to the same memory")
		{
			mystring::String<char> ss1("111111111111111");
			s2 = ss1;
			s3 = ss1;
			REQUIRE(s2 == ss1);
		}

	}
}


SCENARIO("Try other functions with long string")
{
	GIVEN("Two string")
	{
		mystring::String<char> s1("1111111111111111111111111111111111111");
		mystring::String<char> s2("2222222222222222222222222222222222222222");
		mystring::String<char> s3("1111111111111111111111111111111111111");
		char* c1 = "1111111111111111111111111111111111111";
		char* c2 = "2222222222222222222222222222222222222222";
		REQUIRE(s2 != s3);
		REQUIRE(s1 == s3);
		REQUIRE(!(s1 != s3));
		REQUIRE(s1 == s1);
		REQUIRE(s1.length() == strlen(c1));
		REQUIRE(s1.count() == 1);
		WHEN("Concatenate strings(s1=s2+s3)")
		{
			mystring::String<char> s4;
			s4 = s1 + s2;
			char* c4 = "11111111111111111111111111111111111112222222222222222222222222222222222222222";
			REQUIRE(s4 == c4);
			REQUIRE(s4.length() == strlen(c4));
			THEN("trying access to index")
			{
				REQUIRE(s4[2] == '1');
			}
		}
		WHEN("Concatenate strings(s1+=s2)")
		{
			mystring::String<char> s4;
			s1 += s2;
			char* c4 = "11111111111111111111111111111111111112222222222222222222222222222222222222222";
			REQUIRE(s1 == c4);
			REQUIRE(s1.length() == strlen(c4));
			THEN("trying access to index")
			{
				REQUIRE(s1[2] == '1');
			}
		}
		WHEN("Few string have access to the same memory")
		{
			mystring::String<char> ss1("1111122222222221111111111111111111111111111111");
			s2 = ss1;
			s3 = ss1;
			REQUIRE(s2 == ss1);
			REQUIRE(ss1.count() == 3);

		}

	}
}
SCENARIO("Try other functions with long and short string")
{
	GIVEN("Two string")
	{
		mystring::String<char> s1("1111111111111111111111111111111111111");
		mystring::String<char> s2("234");
		mystring::String<char> s3("1111111111111111111111111111111111111");
		char* c1 = "1111111111111111111111111111111111111";
		char* c2 = "234";
		REQUIRE(s2 != s3);
		REQUIRE(s1 == s3);
		REQUIRE(!(s1 != s3));
		REQUIRE(s1 == s1);
		REQUIRE(s1.length() == strlen(c1));
		REQUIRE(s1.count() == 1);
		WHEN("Concatenate strings(s1=s2+s3)")
		{
			mystring::String<char> s4;
			s4 = s1 + s2;
			char* c4 = "1111111111111111111111111111111111111234";
			REQUIRE(s4 == c4);
			REQUIRE(s4.length() == strlen(c4));
			THEN("trying access to index")
			{
				REQUIRE(s4[2] == '1');
			}
		}
		WHEN("Concatenate strings(s1+=s2)")
		{
			mystring::String<char> s4;
			s1 += s2;
			char* c4 = "1111111111111111111111111111111111111234";
			REQUIRE(s1 == c4);
			REQUIRE(s1.length() == strlen(c4));
			THEN("trying access to index")
			{
				REQUIRE(s1[2] == '1');
			}
		}
	}
}




//int main()
//{
//	setlocale(LC_ALL, "rus");
////	mystring::String<char> s1;
//	mystring::String<char> s1("222222222222222222");
//	mystring::String<char> s2("1333");
//	mystring::String<char> s3(s2);
////	s1 = s2;
////	std::cin >> s1;
//	s1 += s2;
//
//	std::string a = "123";
//	auto z = a.begin();
//std::	cout << s1;
//for (auto i = s1.begin(); i != s1.end(); ++i)
//	std::cout << *i;
//			char* c1 = "1234";
//			char* c2 = "567";
//			//std::cout<<s1.str();
////			mystring::StrCmp<char>(s1.str(), c1);
//
//
//	mystring::String<char> s("123");
//	mystring::String<char> ss("1234");
//	mystring::String<char> d(s);
////	d = s+ss;
////	s =s+ ss;
//	s = ss + s;
//	s = ss + s;
////	std::cin >> s;
//std::cout << s;
////	std::cout<<mystring::StrCmp<char>(s.difstr->str_ , "1234");
//	//std::cout <<s;
//	system("pause");
//	return 0;
//}	